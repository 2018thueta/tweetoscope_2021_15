import numpy as np
import scipy.optimize as optim
import json
from kafka import KafkaProducer
from kafka import KafkaConsumer
import pandas as pd
import ast
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
import pickle
import sys
import logger

if __name__ == "__main__":

    logger = logger.get_logger('learner', broker_list='localhost:9092', debug=True)

    in_topic = "samples"
    out_topic = "models"

    # Create Consumer
    consumer = KafkaConsumer(in_topic, 
                            bootstrap_servers = "localhost:9092", 
                            value_deserializer=lambda v: ast.literal_eval(v.decode('utf-8')))

    # Create Producer message -> pickle
    producer = KafkaProducer(bootstrap_servers = 'localhost:9092',
                             key_serializer = str.encode,
                             value_serializer=lambda v: pickle.dumps(v))

    X = {600:[], 1200:[]}
    W = {600:[], 1200:[]}
    logger.info(type(X))
    
    for msg in consumer :
        T_obs = int(msg.key)
        msg = msg.value
        xt = X[T_obs]
        wt = W[T_obs]
        xt.append(msg['X'])
        wt.append(msg['W'])
        X[T_obs] = xt
        W[T_obs] = wt

        if len(xt)%20 == 0 or len(xt)<3 :
            forest_regr = RandomForestRegressor(n_estimators=50)
            forest = forest_regr.fit(xt,wt)
            producer.send(out_topic, key = str(T_obs), value = forest)
            logger.info("message sent to model topic")

    producer.flush()