# Learner

This folder contains all the file necessary to execute the **Learner** : 

The **Learner** uses the partial and complete cascades in topics **`cascade_properties`** and **`cascade_series`** in order to train a Random Forest classifier and sends the model in the topic **`models`**.

### Role of each file :
- Dockerfile.Learner is the Dockerfile to run the container for the learner
- learner.py is the main python script to run the learner
- logger.py sends logs from the learner in the topic **`logs`**
