openjdk >= 11.0.11
kafka >= 2.12-2.4.1
build-essential >= 12.8
pkg-config >= 0.29.1-0
python-dev >= 2.7.17-4
python-six >= 1.14.0-2
cython >= 0.29.14-0.1
python-numpy
coinor-libipopt1v5 >= 3.11.9-2.2
coinor-libipopt-dev >= 3.11.9-2.2
librdkafka >= 0.9.4
CMake >= 3.9.2
libboost-all-dev >= 1.71.0
docker
