##### Imports

#import from Hawkes Estimator 
from Hawkes import compute_MLE
from Hawkes import compute_MAP
from Hawkes import prediction
#logger
import logger
#External exports 
import numpy as np
#External imports for kafka producer and consumer 
import json                       # To parse and dump JSON
from kafka import KafkaProducer   # Import Kafka producder
import ast
from kafka import KafkaConsumer


logger = logger.get_logger('estimator', broker_list='localhost:9092', debug=True)  # the source string (here 'my-node') helps to identify


producer = KafkaProducer(
  bootstrap_servers = "localhost:9092",                     # List of brokers passed from the command line
  value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
  key_serializer=lambda v : json.dumps(v).encode('utf-8')
)

consumer = KafkaConsumer('cascade_series',                   # Topic name
  bootstrap_servers = "localhost:9092",                        # List of brokers passed from the command line
  value_deserializer=lambda v: ast.literal_eval(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
  auto_offset_reset="earliest"
)
#add a counter to the number of received cascades
nb_received_cascades=0
#Reading cascades from topic consumer
for received in consumer:
    #send a warning message if received is empty
    if len(received)==0:
        logger.warning("Didn't receive anything")
    nb_received_cascades=nb_received_cascades+1
    #Recuperate the value
    msg = received.value
    #recuperate the needed attributes from the value
    T_obs = msg['T_obs']
    casc = msg['tweets']
    cascade = np.asarray(casc)
    n_obs = len(cascade)
    cid = msg['cid']
    tweet_msg = msg['msg']
    
    #send  log messages
    logger.info("Just received cascade of tweet: "+cid+" and of observation time: "+str(T_obs))
    logger.info("Number of cascades received till now: "+str(nb_received_cascades))
    
    # Alpha and mu
    mu = 10
    alpha = 2.3
    #set the origin of the cascade to 0
    cascade[:,0] = cascade[:,0] - cascade[0][0]
    #Compute the estimation of parameters p,beta
    res1, res2 = compute_MAP(cascade, T_obs, alpha, mu)
    p = res2[0]
    beta = res2[1]
    params = [p,beta]
    # Prediction
    final_cascade_size = prediction(params, cascade, alpha, mu, T_obs)
    n_supp = round(final_cascade_size)
    # Computing parameter n_star
    n_star = p * mu * (alpha-1)/(alpha-2)
    # Computing parameter G1
    I = cascade[:,0] < T_obs
    tis = cascade[I,0]
    mis = cascade[I,1]
    G1 = p * np.sum(mis * np.exp(-beta * (T_obs - tis))) 
    # Prepare the JSON message to send
    msg_to_send = {
        'type': 'params',
        'cid': cid,
        'msg': tweet_msg,
        'n_obs': n_obs,
        'n_supp':n_supp,
        'params': params,
        'n_star': n_star,
        'G1':G1
        }
    #Sending log message
    logger.info("Sending to cascade_properties") 
    # Sending a JSON message to the topic cascade_properties 
    producer.send('cascade_properties', key = T_obs, value = msg_to_send) 
logger.info("total number of received cascades: " +str(nb_cascade_series)) 
producer.flush() # Flush: force purging intermediate buffers before leaving
