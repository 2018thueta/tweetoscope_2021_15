# Estimator

This folder contains all the file necessary to execute the Collector : 

<img src="https://pennerath.pages.centralesupelec.fr/tweetoscope/graphviz-images/b7e809ef403f840c575254f83657d16b58b0218d.svg
" alt="MarineGEO circle logo" style="height: 100px; width:100px;"/>


The **Estimator** reads the information about the partial cascade in the topic **`cascade_series`** from the **Collector** and writes in the topic **`cascade_properties`** the parameters estimated.

### Role of each file :
- Dockerfile.Estimator is the Dockerfile to run the container for the estimator
- Hawkes.py contain the function that makes the estimation of the parameters
- logger.py module to send log messages
- main.py receve the cascades and use Hawkes.py to send the parameters to the topic `cascade_properties` 
- pyhon_requirements.txt contains the list of libraries required
