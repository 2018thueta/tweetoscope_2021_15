#include <memory>
#include <queue>
#include <list>
#include "Cascade.hpp"

namespace tweetoscope {
    using ref_cascade = std::shared_ptr<Cascade>;
    using weak_cascade = std::weak_ptr<Cascade>;

    class Processor {
        source::idf idf;
        std::vector<std::size_t> t_obs;
        std::size_t terminated;
        std::priority_queue<ref_cascade, std::vector<ref_cascade>> prio_q;
        std::map<std::string, weak_cascade> symbol_table;
        std::map<std::size_t, std::queue<weak_cascade>>  series_map;
        std::size_t min_size;


        public: 
        Processor() = default;
        Processor(tweet twt, std::vector<std::size_t> t_obs, std::size_t min_size) : idf(twt.source), t_obs(t_obs), terminated(t_obs.back()), min_size(min_size){
            symbol_table.clear();
            series_map.clear();
            add_new_tweet(twt);

        }

        void add_tweet (tweet twt){ 
            //adds twt to the corresponding cascade if it exists, and creates a new cascade if needed
            weak_cascade current_cascade = symbol_table[twt.info];
            ref_cascade rc = current_cascade.lock();
            if (rc){ 
                //case where the incoming tweet has an active cascade
                if (rc->get_start_time() + terminated > twt.time){ 
                    //verifying that the cascade is not expired
                    rc->add_tweet(twt);
                }
            }
            else { 
                //case where the tweet doesn't have a corresponding cascade
                if (twt.type == "tweet") {
                    add_new_tweet(twt);  
                    //Creating new cascade for that tweet
                }
                // A retweet for a cascade that doesn't exist is either an error or the cascade has expired. Either nothing has to be done.
            }
        }

        void add_new_tweet (tweet twt){ 
            //This method creates a new cascade and adds twt to it
            Cascade cas(twt.info, twt);
            ref_cascade rc = std::make_shared<Cascade>(cas);
            weak_cascade wc = rc;
            prio_q.push(rc);
            symbol_table.insert_or_assign(twt.info, wc);
            for(auto &t : t_obs){
                series_map[t].push(wc);
            }
        }


        std::list<std::string> get_cascades_series(std::size_t current_time){
            /* this function is called by the Collector.cpp script to get the list of available cascade series
            parameters : 
            - current_time is the time of the latest received tweet and is used to know which cascades are past one observation window
            return :
            - a list of strings, each one is a message from a cascade of one observation window
            */
            std::list<std::string> cascades_string = {""};
            cascades_string.clear();
            for (std::size_t &t : t_obs){
                std::queue<weak_cascade> &cascades_list = series_map[t];
                while (!cascades_list.empty() && cascades_list.front().lock()->get_start_time() + t < current_time){
                    ref_cascade rc = cascades_list.front().lock();
                    if (rc->get_start_time() + t < current_time){
                        cascades_list.pop();  //a cascade past one observation window is removed from the corresponding queue
                        if (rc->get_size() >= min_size){
                            cascades_string.push_back(get_cascade_series(rc, t));
                        }
                    }
                }
            }
            return cascades_string;
        }

        std::string get_cascade_series(ref_cascade rc, std::size_t t){
            /* this function is used to get an observation window message from a single cascade
            The cascade is supposed to be just past the observation window
            parameters : 
            - rc is a pointer to the active cascade
            - t is the size of the observation window
            return :
            - a string which is a message from the cascade for the observation window of size t
            */
            std::vector<std::pair<timestamp,double>> series = rc->get_series();
            std::string series_string = "";
            series_string.append("{ 'type' : 'serie', 'cid': '");
            series_string.append(rc->idf);
            series_string.append("', 'msg' : '");
            series_string.append(rc->msg);
            series_string.append("', 'T_obs': ");
            series_string.append(std::to_string(t));
            series_string.append(", 'tweets' : [ ");
            for (std::pair<timestamp,double> &tweet_value : series){
                series_string.append("(");
                series_string.append(std::to_string(tweet_value.first));
                series_string.append(", ");
                series_string.append(std::to_string(tweet_value.second));
                series_string.append("), ");
            }
            series_string.append("] }");
            
            return series_string;
            }

        std::list<std::string> get_cascades_properties(timestamp current_time){
            /* this function is called by the Collector.cpp script to get the list of available cascade properties
            parameters : 
            - current_time is the time of the latest received tweet and is used to know which cascades are past their expiry time
            return :
            - a list of strings, each one is a message from a terminated cascade
            */
            std::list<std::string> properties_string = {""};
            properties_string.clear();
            while(!prio_q.empty() && prio_q.top()->get_start_time() + terminated < current_time){   //the cascades are ordered by start time in the queue so it is sufficient to look at the first one every time
                ref_cascade rc = prio_q.top();
                if (rc->get_start_time() + terminated < current_time){
                    prio_q.pop();
                    if (rc->get_size() >= min_size){
                        properties_string.push_back(get_cascade_properties(rc));
                    }
                    rc.reset();  //deleting the expired cascade
                }
            }

            return properties_string;
        }

        std::string get_cascade_properties(ref_cascade rc){
            /* this function is used to get a message from an expiring cascade
            parameters : 
            - rc is a pointer to the active cascade
            return :
            - a string which is a message from the expiring cascade
            */
            std::string properties_string = "";
            properties_string.append("{ 'type' : 'size', 'cid': '");
            properties_string.append(rc->idf);
            properties_string.append("', 'n_tot': ");
            properties_string.append(std::to_string(rc->series.size()));
            properties_string.append(", 't_end': ");
            properties_string.append(std::to_string(rc->get_finish_time()));
            properties_string.append(" }");
            return properties_string;
        }




    };
}