# Collector

This folder contains all the files necessary to execute the Collector : 

<img src="https://pennerath.pages.centralesupelec.fr/tweetoscope/graphviz-images/86532ade1202835957f71332aa02ef9a55fe8c0a.svg
" alt="MarineGEO circle logo" style="height: 100px; width:100px;"/>


The **Collector** reads the tweets in the topic **`tweets`** from the **Generator** and writes information about the partial and finite tweet cascades in the topics **`cascade_series`** and **`cascade_properties`**.

### Role of each file :
- Cascade.hpp defines the class cascade allowing us to interact with cascades
- Collector.cpp is the main cpp file
- Dockerfile.Collector is the Dockerfile for the collector
- Processor.hpp defines the class processor, which handles all cascades from one source.
- collector.ini contains some execution parameters
- tweet.hpp defines the class tweet
- tweetoscopeCollectorParams.hpp is used to extract the needed parameters from the collector.ini file
