#include <vector>
#include <string>
#include "tweet.hpp"


namespace tweetoscope{
    struct Cascade {
        std::string idf;
        std::vector<std::pair<timestamp,double>> series;
        std::string msg;
        std::size_t recent_message;

        Cascade(std::string idf):idf(idf), msg(""){
        };
        Cascade(std::string idf, tweet twt):idf(idf), msg(twt.msg){
            add_tweet(twt);
        };
        ~Cascade()=default;


        void add_tweet(tweet t){
            // This method adds the tweet t to the cascade
            series.push_back({t.time, t.magnitude});
            recent_message = t.time;
        };

        std::size_t get_size(){
            return series.size();
        }

        std::size_t get_start_time(){
            return series[0].first;}

        std::size_t get_finish_time(){
            return series.back().first;}

        std::vector<std::pair<timestamp,double>> get_series(){return series;};
        
        bool operator<(Cascade c){
            // This operator is created to order the cascades in the priority_queue in Processor.hpp
            return get_start_time() < c.get_start_time();
        }
    };
}