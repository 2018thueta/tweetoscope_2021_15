#include <cppkafka/cppkafka.h>
#include <map>
#include <list>
#include "tweet.hpp"
#include "Processor.hpp"
#include "tweetoscopeCollectorParams.hpp"

/* Compile with the following :
 g++ -std=c++17 -o Collector Collector.cpp `pkg-config --libs --cflags cppkafka`
*/

int main(int argc, char* argv[]) {

    // Verification of the correct number of arguments
    if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
    }

    // Getting the parameters from the .ini file
    tweetoscope::params::collector params(argv[1]);
    auto t_obs = params.times.observation;
    std::size_t t_ter = params.times.terminated;
    t_obs.push_back(t_ter);

    std::size_t min_size = params.cascade.min_cascade_size;


    // Initializing the cppkafka objects.
    cppkafka::Configuration config = {
        { "bootstrap.servers", params.kafka.brokers },
	    { "auto.offset.reset", "earliest" },
	    { "group.id", "myOwnPrivateCppGroup" },
	    {"log.connection.close", false },
        {"metadata.broker.list", params.kafka.brokers }
        };
    cppkafka::Consumer consumer (config);
    consumer.subscribe({params.topic.in});
    cppkafka::MessageBuilder series_builder {params.topic.out_series};  
    cppkafka::Producer producer (config);
    cppkafka::MessageBuilder properties_builder {params.topic.out_properties}; 
    

    std::map<tweetoscope::source::idf, tweetoscope::Processor> Processor_map;

    //routine loop
    while(true){

        auto msg = consumer.poll();  
        if (msg && ! msg.get_error()) {
            tweetoscope::tweet twt;
            auto istr = std::istringstream(std::string(msg.get_payload()));
            istr >> twt;        
            auto current_source = twt.source;

            //finding or creating the processor for the current source, and giving the tweet to the processor
            //the processor will then add the tweet to the right cascade
            if (Processor_map.find(current_source) != Processor_map.end()){
                Processor_map[current_source].add_tweet(twt);
            }
            else {
                if (twt.type == "tweet"){
                    tweetoscope::Processor p = tweetoscope::Processor (twt, t_obs, min_size);
                    Processor_map.insert_or_assign(current_source, p);
                }
            }

            //receiving partial and complete cascades from the active processor
            if (Processor_map.find(current_source) != Processor_map.end()){
                std::list<std::string> series = Processor_map[current_source].get_cascades_series(twt.time);
                std::list<std::string> properties = Processor_map[current_source].get_cascades_properties(twt.time);

                for (auto &cas_series : series){
                    std::string out_key = "None";
                    series_builder.key(out_key);
                    std::ostringstream ostr;
                    ostr << cas_series;
                    auto msg = ostr.str();
                    series_builder.payload(msg);
                    producer.produce(series_builder);
                }

                for (auto &cas_properties : properties){
                    for (auto &t: t_obs){
                        std::string out_key = std::to_string(t);
                        properties_builder.key(out_key);
                        std::ostringstream ostr;
                        ostr << cas_properties;
                        auto msg = ostr.str();
                        properties_builder.payload(msg);
                        producer.produce(properties_builder);
                    }

                }

            }
        }
    }
    return 0;
}