# tweetoscope_2021_15

## Introduction

This repository contains all the work done by by Malak ELFLITTY, Lucas MLYNARZ and Alban THUET for a 3rd year Engineer School project in CentraleSupélec.

The goal of this projet was to to guess the final number of retweets just by observing the first rewtweets that appeared.


## Organisation

To do so, we organised the project in multiple parts that are linked according to this graph

<img src="https://pennerath.pages.centralesupelec.fr/tweetoscope/graphviz-images/ead74cb4077631acad74606a761525fe2a3228c1.svg
" alt="MarineGEO circle logo" style="height: 100px; width:100px;"/>

## Summary

 - Setting up
 - Generator
 - Collector
 - Estimator
 - Predictor
 - Learner
 - Logger

Each part corresponds to a specific task in the project. There is a folder in the repository for each one with a dedicated README.

# How to RUN the pipeline

Follow all the command line below to launch all the pipeline

### Clone repository
``` 
git clone https://gitlab-student.centralesupelec.fr/2018thueta/tweetoscope_2021_15.git
``` 
``` 
cd tweetoscope_2021_15
``` 

### Start zookeeper
```
sudo docker run --rm -d --name zookeeper --network host zookeeper
```

### Start kafka broker
```
sudo docker run --rm -d --name kafka --network host \
	--env KAFKA_BROKER_ID=0 \
	--env KAFKA_LISTENERS=PLAINTEXT://:9092 \
	--env KAFKA_ZOOKEEPER_CONNECT=localhost:2181 \
	--env KAFKA_CREATE_TOPICS="tweets:4:1,cascade_series:1:1,cascade_properties:1:1,samples:1:1,stat:1:1"  \
	wurstmeister/kafka
``` 

### Run Generator
```
cd Generator
```
```
sudo docker build -t generator -f Dockerfile.Generator .
```
```
sudo docker run --rm -d --name generator --network host generator
```

### Run **collector**
```
cd ../Collector
```
```
sudo docker build -t collector -f Dockerfile.Collector .
```
```
sudo docker run --rm -d --name collector --network host collector
```

### Run **Estimator**
```
cd ../Estimator
```
```
sudo docker build -t estimator -f Dockerfile.Estimator .
```
```
sudo docker run --rm -d --name estimator --network host estimator
```

### Run **Predictor**
```
cd ../Predictor
```
``` 
sudo docker build -t predictor -f Dockerfile.Predictor .
```
```
sudo docker run --rm -d --name predictor --network host predictor
```

### Run **Learner**
```
cd ../Learner
```
```
sudo docker build -t learner -f Dockerfile.Learner .
```
```
sudo docker run --rm -d --name learner --network host learner
```
