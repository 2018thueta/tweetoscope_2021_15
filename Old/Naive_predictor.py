import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producder
import ast


consumer = KafkaConsumer('cascade_properties',                   # Topic name
  bootstrap_servers = "localhost:9092",                        # List of brokers passed from the command line
  value_deserializer=lambda v: ast.literal_eval(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
  key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
)


mu = 10; alpha = 2.3; # Fixed parameters of the model 

def prediction(p, beta, G1, alpha, mu, t, n_obs):
    """
    Returns the expected total numbers of points for a set of time points
    
    params   -- parameter (p,beta,G1) of the Hawkes process 
    alpha    -- power parameter of the power-law mark distribution
    mu       -- min value parameter of the power-law mark distribution
    t        -- current time (i.e end of observation window)
    n_obs    -- number of tweets observed at the time t
    """
    
    EM = mu * (alpha - 1) / (alpha - 2)
    n_star = p * EM
    if n_star >= 1:
        raise Exception(f"Branching factor {n_star:.2f} greater than one")
    Ntot = n_obs + G1 / (1. - n_star)
    return Ntot, G1

for msg in consumer:                            # Blocking call waiting for a new message
    if msg.value['type'] == 'params':
        t = msg.key                                 # Current time
        n_obs = msg.value['n_obs']                  # Number of tweets observed at the time t
        p,beta = msg.value['params']                # parameters (p,beta,G1) of the Hawkes process 
        G1 = msg.value['G1']
        pred = prediction(p, beta, G1, alpha, mu, t, n_obs) # predicted number of retweets 

        print ('Time :', t)                         # Write time the received message
        print ('N_Observations :', int(n_obs))      # Write number of observations of the received message
        print ('Predictions :', pred)               # Write predictions of the naive predictor

#     producer = KafkaProducer(
#       bootstrap_servers = "localhost:9092",                     # List of brokers passed from the command line
#       value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
#       key_serializer=str.encode                                 # How to serialize the key
#     )

#     msg = {
#         'dst': 'Metz',
#         'temp': 2,
#         'type': 'rain',
#         'comment': 'Nothing special'
#     }
#     for _ in range(3):
#         producer.send('weather-forecast', key = msg['dst'], value = msg) # Send a new message to topic

#     producer.flush() # Flush: force purging intermediate buffers before leaving
