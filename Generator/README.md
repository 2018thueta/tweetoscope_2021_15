# Generator

This folder contains all the file necessary to execute the Generator : 

<img src="https://pennerath.pages.centralesupelec.fr/tweetoscope/graphviz-images/9d5fef611d89e75acf48ee89d17d22eb78483691.svg" alt="MarineGEO circle logo" style="height: 100px; width:100px;"/>


The **Generator** uses the tweets in files `news-data.csv` and `nexs-index.csv` in order to generate cascades of **tweets** and write it in a kafka topic called **`tweets`** 

### Role of each file :
- Dockerfile.Generator is the Dockerfile to run the container for the generator
- cascades.idx and tweets.idx these index files are used to access the data files efficiently.
- news-data.csv and news-index.csv contain archived tweets that are used to simulate the generation of new ones
- params.config contain the parameters used by the file tweet-generator.cpp
- tweet-generator.cpp is the main cpp file
