import json
from os import wait                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producder
import ast
import logger
import pickle

samples_topic = "samples"
in_topic = "cascade_properties"
models_topic = "models"
stat_topic = "stat"

consumer = KafkaConsumer(in_topic,                   # Topic name
                         # List of brokers passed from the command line
                         bootstrap_servers="localhost:9092",
                         # How to deserialize the value from a binary buffer
                         value_deserializer=lambda v: ast.literal_eval(
                             v.decode('utf-8')),
                         # How to deserialize the key (if any)
                         key_deserializer=lambda v: v.decode(),
                         auto_offset_reset="earliest"
                         )

consumer_models = KafkaConsumer(models_topic,                   # Topic name
                                # List of brokers passed from the command line
                                bootstrap_servers="localhost:9092",
                                value_deserializer =lambda m: pickle.loads(m),
                                consumer_timeout_ms=3000
                                )

producer = KafkaProducer(
            bootstrap_servers = "localhost:9092",                     # List of brokers passed from the command line
            key_serializer = str.encode,
            value_serializer = lambda v: json.dumps(v).encode('utf-8')
            )


mu = 10
alpha = 2.3  # Fixed parameters of the model
EM = mu * (alpha - 1) / (alpha - 2)

received_cids = dict()
waiting_cids = dict()
msg_models = []
logger = logger.get_logger('predictor', broker_list='localhost:9092', debug=True)


def treatment(real_n_tot, msg, t):
    n_obs = msg['n_obs']
    p, beta = msg['params']
    G1 = msg['G1']
    n_star = p*EM
    X = [p, n_star, G1, n_obs, beta]

    try:                            # check case omega is zero
        real_W = (real_n_tot - n_obs) * (1 - n_star) / G1
    except ZeroDivisionError as err:
        W_true = -1

    sample_msg = {'type': 'samples', 'cid': cid, 'X': X, 'W': real_W}
    producer.send(samples_topic, key=t, value=sample_msg)
    
    # Getting the forest sent to the topic models.
    for msg_model in consumer_models:
        msg_models.append(msg_model.value)
    if (len(msg_models) == 0) :
        return None
    msg_model = msg_models[-1]
    W_model = msg_model.predict([X])
    n_model = round(float(W_model * G1 / (1-n_star))) + n_obs

    ARE = abs(n_model - real_n_tot) / real_n_tot
    if ARE <= 1:
        stat_msg = {'type': 'stat', 'cid': cid, 'T_obs': t, 'n_predict': int(n_model), 'n_real': int(real_n_tot), 'ARE': float(ARE)}
        producer.send(stat_topic, key=t, value=stat_msg)



for msg in consumer:                            # Blocking call waiting for a new message

    t = msg.key                                 # Current time
    msg = msg.value
    cid = msg['cid']

    if int(t) != 1800 : 

        logger.info(t)

        if msg['type'] == 'params':

            n_obs = msg['n_obs']
            p, beta = msg['params']
            G1 = msg['G1']

            if cid in received_cids:
                real_n_tot = received_cids[cid]
                treatment(real_n_tot, msg, t)

            else:
                waiting_cids[cid] = msg

        else:
            real_n_tot = msg['n_tot']
            received_cids[cid] = real_n_tot

            if cid in waiting_cids:
                msg = waiting_cids[cid]
                treatment(real_n_tot, msg, t)


producer.flush()  # Flush: force purging intermediate buffers before leaving

