# Predictor

This folder contains all the files necessary to execute the Predictor : 

<img src="https://pennerath.pages.centralesupelec.fr/tweetoscope/graphviz-images/7db8bcb855369981a7297c8a651072f89927a4df.svg
" alt="MarineGEO circle logo" style="height: 100px; width:100px;"/>


The **Predictor** reads the tweets in **`cascade_properties`** from the **Collector** and the **Estimator**, and uses the model in **`models`** from the **Learner** to make a prediction about the total number of retweets in the cascade. Then it publishes the results in the topic **`stats`** and send the information to the **Learner** in the topic **`stats`**.

### Role of each file :
- Dockerfile.Predictor is the Dockerfile for the predictor
- Predictor.py uses the information in `casca_properties` and `models` in order to make a prediction about the number of retweets
- logger.py modules to send log messages from the **Predictor** on the topic `logs`
